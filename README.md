# ToDo List API

The ToDo List API is a simple yet powerful project management tool, designed to help you keep track of your tasks and
accomplish your goals effectively. This RESTful API allows you to manage users and their corresponding To-Do Lists and
Items within those lists. The application is built using Java 17, Spring Boot, JPA, and PostgreSQL Database.

## Features

- Manage Users and their To-Do Lists.
- Manage Items in each To-Do List.
- Create, Update, and Delete Users, To-Do Lists, and Items within Lists.
- Query Users based on their email addresses.
- Easy Swagger UI integration for API documentation and testing.
- Uses PostgreSQL as the database.

## Getting Started

To run this API on your local machine, follow the instructions provided below.

### Prerequisites

Before you start, make sure you have the following software installed on your machine.

- Java 17
- Apache Maven 3.6.0 or newer
- Git
- PostgreSQL

### Steps to Install and Run

1. Install and set up PostgreSQL on your machine. Create a new database named `todo_db` and a new user `db_user` with
   the password `makePassword`.

2. Clone the repository:

```bash
git clone https://gitlab.com/alina_andreeva/todo-list.git
```

3. Change the directory:

```bash
cd todo-list
```

4. Open the `application.properties` file and update the database connection details according to your PostgreSQL
   configuration.

5. Build the project using Maven:

```bash
mvn clean install
```

6. Run the built project:

```bash
mvn spring-boot:run
```

Now, the API is up and running on your local machine and is available at http://localhost:8080/swagger-ui/. The ToDo
List API uses PostgreSQL as the database.

### API Endpoints

Use the following RESTful API endpoints to interact with the application:

- User
    - GET /users/{id} - Retrieve a User by their ID
    - GET /users/email?email={email} - Retrieve a User by their email address
    - POST /users - Create a new User

- ToDoList
    - GET /lists/{id} - Retrieve a ToDo List by its ID
    - POST /lists - Create a new ToDo List
    - DELETE /lists/{id} - Delete a ToDo List by its ID

- Item
    - GET /items?todoListId={todoListId} - Retrieve Items by ToDo List ID
    - GET /items/{id} - Retrieve an Item by its ID
    - POST /items - Create a new Item
    - PUT /items/{id} - Update an Item by its ID
    - DELETE /items/{id} - Delete an Item by its ID

## Testing the API endpoints

The application is integrated with Swagger UI for easy API documentation and testing.
Visit http://localhost:8080/swagger-ui/ to view the API documentation and interact with the API directly from the
browser.

Alternatively, you can also use tools like [Postman](https://www.postman.com/), [Insomnia](https://insomnia.rest/), or
any other API testing tool.

Feedback:

- Was it easy to complete the task using AI?
  It was fine

- How long did task take you to complete? (Please be honest, we need it to gather anonymized statistics)
  It took approximately 4 hours

- Was the code ready to run after generation? What did you have to change to make it usable?
  The code was runnable, but it was needed to remove unusable things, change names, and even reorganize structure.
  To be honest, I think that I would write the code for such an easy task faster and better without AI. Because for such
  task I don't need any assistance, but during the course, I was compelled to spend a lot of time communicating with AI
  and refactor generated code.

- Which challenges did you face during completion of the task?
  It was uncomfortable to keep in mind thread of conversation with AI. It felt like distraction from the work.
  Nevertheless, I like using AI for learning and solving specific tasks.

- Which specific prompts you learned as a good practice to complete the task?
  I found the structure of prompt to force AI generate valid unit tests.
