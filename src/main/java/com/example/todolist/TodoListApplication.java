package com.example.todolist;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(info = @Info(title = "ToDo", version = "1.0", description = "To-do list"))
@SpringBootApplication
public class TodoListApplication {

  public static void main(String[] args) {
    SpringApplication.run(TodoListApplication.class, args);
  }
}
