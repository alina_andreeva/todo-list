package com.example.todolist.controller;

import com.example.todolist.domain.Item;
import com.example.todolist.service.ItemService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/items")
@RequiredArgsConstructor
public class ItemController {

  private final ItemService itemService;

  @GetMapping
  public List<Item> getItemsByTodoListId(@RequestParam(name = "todoListId") Long todoListId) {

    return itemService.getItemsByTodoListId(todoListId);
  }

  @GetMapping("/{id}")
  public Item getItem(@PathVariable Long id) {

    return itemService.getItemById(id);
  }

  @PostMapping
  public Item createItem(@RequestBody Item item) {

    return itemService.createItem(item);
  }

  @PutMapping("/{id}")
  public Item updateItem(@PathVariable Long id, @RequestBody Item itemDetails) {

    return itemService.updateItem(id, itemDetails);
  }

  @DeleteMapping("/{id}")
  public void deleteItem(@PathVariable Long id) {

    itemService.deleteItem(id);
  }
}
