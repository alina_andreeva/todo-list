package com.example.todolist.controller;

import com.example.todolist.domain.ToDoList;
import com.example.todolist.service.ToDoListService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/lists")
public class ToDoListController {

  private final ToDoListService toDoListService;

  @GetMapping("/{id}")
  public ToDoList getToDoList(@PathVariable Long id) {

    return toDoListService.getToDoListById(id);
  }

  @PostMapping
  public ToDoList createToDoList(@RequestBody ToDoList toDoList) {

    return toDoListService.createToDoList(toDoList);
  }

  @DeleteMapping("/{id}")
  public void deleteToDoList(@PathVariable Long id) {

    toDoListService.deleteToDoList(id);
  }
}