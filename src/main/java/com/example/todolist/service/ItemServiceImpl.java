package com.example.todolist.service;

import com.example.todolist.domain.Item;
import com.example.todolist.repository.ItemRepository;
import java.util.List;
import java.util.NoSuchElementException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {

  private final ItemRepository itemRepository;

  @Override
  public List<Item> getItemsByTodoListId(Long todoListId) {

    return itemRepository.findAllByTodoListId(todoListId);
  }

  @Override
  public Item getItemById(Long id) {

    return itemRepository.findById(id)
        .orElse(null);
  }

  @Override
  public Item createItem(Item item) {

    return itemRepository.save(item);
  }

  @Override
  public Item updateItem(Long id, Item itemDetails) {

    Item item = itemRepository.findById(id)
        .orElseThrow(() -> new NoSuchElementException("Item not found"));

    item.setTitle(itemDetails.getTitle());
    item.setDescription(itemDetails.getDescription());

    return itemRepository.save(item);
  }

  @Override
  public void deleteItem(Long id) {

    itemRepository.deleteById(id);
  }
}
