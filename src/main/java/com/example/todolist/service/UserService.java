package com.example.todolist.service;

import com.example.todolist.domain.User;

public interface UserService {

  User getUserByEmail(String email);

  User getUserById(Long id);

  User createUser(User user);
}
