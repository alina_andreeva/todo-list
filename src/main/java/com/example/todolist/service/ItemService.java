package com.example.todolist.service;

import com.example.todolist.domain.Item;
import java.util.List;

public interface ItemService {

  List<Item> getItemsByTodoListId(Long todoListId);

  Item getItemById(Long id);

  Item createItem(Item item);

  Item updateItem(Long id, Item itemDetails);

  void deleteItem(Long id);
}
