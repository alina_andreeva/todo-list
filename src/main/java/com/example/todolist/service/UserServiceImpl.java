package com.example.todolist.service;

import com.example.todolist.domain.User;
import com.example.todolist.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  @Override
  public User getUserByEmail(String email) {

    return userRepository.findUsersByEmail(email);
  }

  @Override
  public User getUserById(Long id) {
    return userRepository.findById(id).orElse(null);
  }

  @Override
  public User createUser(User user) {

    return userRepository.save(user);
  }
}
