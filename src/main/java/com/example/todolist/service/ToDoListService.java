package com.example.todolist.service;

import com.example.todolist.domain.ToDoList;

public interface ToDoListService {

  ToDoList getToDoListById(Long id);

  ToDoList createToDoList(ToDoList toDoList);

  void deleteToDoList(Long id);
}
