package com.example.todolist.service;

import com.example.todolist.domain.ToDoList;
import com.example.todolist.repository.ToDoListRepository;
import java.util.NoSuchElementException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ToDoListServiceImpl implements ToDoListService {

  private final ToDoListRepository toDoListRepository;

  @Override
  public ToDoList getToDoListById(Long id) {

    return toDoListRepository.findById(id)
        .orElseThrow(() -> new NoSuchElementException("Todo list not Found"));
  }

  @Override
  public ToDoList createToDoList(ToDoList toDoList) {

    return toDoListRepository.save(toDoList);
  }

  @Override
  public void deleteToDoList(Long id) {

    toDoListRepository.deleteById(id);
  }
}
