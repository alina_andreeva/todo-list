package com.example.todolist.repository;

import com.example.todolist.domain.Item;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

  List<Item> findAllByTodoListId(Long toDoListId);
}
