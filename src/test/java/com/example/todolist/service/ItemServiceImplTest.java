package com.example.todolist.service;

import com.example.todolist.domain.Item;
import com.example.todolist.domain.ToDoList;
import com.example.todolist.repository.ItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ItemServiceImplTest {

  @InjectMocks
  private ItemServiceImpl itemService;

  @Mock
  private ItemRepository itemRepository;

  private Item item1;
  private Item item2;

  @BeforeEach
  void setUp() {
    ToDoList todoList = new ToDoList();
    todoList.setId(1L);
    item1 = new Item(1L, "Test item 1", "Description of the test item", todoList);
    item2 = new Item(2L, "Test item 2", "Description of the test item", todoList);
  }

  @Test
  public void getItemsByTodoListId() {
    when(itemRepository.findAllByTodoListId(1L)).thenReturn(Arrays.asList(item1, item2));

    List<Item> items = itemService.getItemsByTodoListId(1L);

    assertThat(items).isNotEmpty();
    assertThat(items).hasSize(2);
    assertThat(items.get(0).getTitle()).isEqualTo("Test item 1");
    assertThat(items.get(1).getTitle()).isEqualTo("Test item 2");
  }

  @Test
  public void getItemById() {
    when(itemRepository.findById(1L)).thenReturn(Optional.of(item1));

    Item item = itemService.getItemById(1L);

    assertThat(item).isNotNull();
    assertThat(item.getId()).isEqualTo(1L);
    assertThat(item.getTitle()).isEqualTo("Test item 1");
  }

  @Test
  public void createItem() {
    when(itemRepository.save(item1)).thenReturn(item1);

    Item item = itemService.createItem(item1);

    assertThat(item).isNotNull();
    assertThat(item.getId()).isEqualTo(1L);
    assertThat(item.getTitle()).isEqualTo("Test item 1");
  }

  @Test
  public void updateItem() {
    when(itemRepository.findById(1L)).thenReturn(Optional.of(item1));
    when(itemRepository.save(item1)).thenReturn(item1);

    Item updatedItem = new Item(1L, "Updated item", "Updated description", null);
    itemService.updateItem(1L, updatedItem);

    verify(itemRepository).save(item1);
    assertThat(item1.getTitle()).isEqualTo("Updated item");
    assertThat(item1.getDescription()).isEqualTo("Updated description");
  }

  @Test
  public void deleteItem() {
    Long itemId = 1L;
    doNothing().when(itemRepository).deleteById(itemId);

    itemService.deleteItem(itemId);

    verify(itemRepository, times(1)).deleteById(itemId);
  }
}