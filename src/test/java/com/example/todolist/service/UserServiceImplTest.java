package com.example.todolist.service;

import com.example.todolist.domain.User;
import com.example.todolist.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

  @InjectMocks
  private UserServiceImpl userService;

  @Mock
  private UserRepository userRepository;

  private User user1;

  @BeforeEach
  void setUp() {
    user1 = User.builder()
        .id(1L)
        .username("testuser")
        .email("test@example.com")
        .todoLists(null)
        .build();
  }

  @Test
  public void getUserByEmail() {
    when(userRepository.findUsersByEmail("test@example.com")).thenReturn(user1);

    User user = userService.getUserByEmail("test@example.com");

    assertThat(user).isNotNull();
    assertThat(user.getId()).isEqualTo(1L);
    assertThat(user.getUsername()).isEqualTo("testuser");
    assertThat(user.getEmail()).isEqualTo("test@example.com");
  }

  @Test
  public void getUserById() {
    when(userRepository.findById(1L)).thenReturn(Optional.of(user1));

    User user = userService.getUserById(1L);

    assertThat(user).isNotNull();
    assertThat(user.getId()).isEqualTo(1L);
    assertThat(user.getUsername()).isEqualTo("testuser");
    assertThat(user.getEmail()).isEqualTo("test@example.com");
  }

  @Test
  public void createUser() {
    when(userRepository.save(user1)).thenReturn(user1);

    User user = userService.createUser(user1);

    assertThat(user).isNotNull();
    assertThat(user.getId()).isEqualTo(1L);
    assertThat(user.getUsername()).isEqualTo("testuser");
    assertThat(user.getEmail()).isEqualTo("test@example.com");
  }
}