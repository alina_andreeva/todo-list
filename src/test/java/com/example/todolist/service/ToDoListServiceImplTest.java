package com.example.todolist.service;

import com.example.todolist.domain.ToDoList;
import com.example.todolist.domain.User;
import com.example.todolist.repository.ToDoListRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ToDoListServiceImplTest {

  @InjectMocks
  private ToDoListServiceImpl toDoListService;

  @Mock
  private ToDoListRepository toDoListRepository;

  private ToDoList toDoList1;

  @BeforeEach
  void setUp() {
    User user = User.builder()
        .id(1L)
        .username("testuser")
        .email("test@example.com")
        .todoLists(null)
        .build();

    toDoList1 = ToDoList.builder()
        .id(1L)
        .name("Test ToDoList")
        .user(user)
        .items(null)
        .build();
  }

  @Test
  public void getToDoListById() {
    when(toDoListRepository.findById(1L)).thenReturn(Optional.of(toDoList1));

    ToDoList toDoList = toDoListService.getToDoListById(1L);

    assertThat(toDoList).isNotNull();
    assertThat(toDoList.getId()).isEqualTo(1L);
    assertThat(toDoList.getName()).isEqualTo("Test ToDoList");
  }

  @Test
  public void getToDoListById_NotFound() {
    when(toDoListRepository.findById(1L)).thenReturn(Optional.empty());

    assertThatThrownBy(() -> toDoListService.getToDoListById(1L))
        .isInstanceOf(NoSuchElementException.class)
        .hasMessage("Todo list not Found");
  }

  @Test
  public void createToDoList() {
    when(toDoListRepository.save(toDoList1)).thenReturn(toDoList1);

    ToDoList toDoList = toDoListService.createToDoList(toDoList1);

    assertThat(toDoList).isNotNull();
    assertThat(toDoList.getId()).isEqualTo(1L);
    assertThat(toDoList.getName()).isEqualTo("Test ToDoList");
  }

  @Test
  public void deleteToDoList() {
    Long toDoListId = 1L;
    doNothing().when(toDoListRepository).deleteById(toDoListId);

    toDoListService.deleteToDoList(toDoListId);

    verify(toDoListRepository, times(1)).deleteById(toDoListId);
  }
}