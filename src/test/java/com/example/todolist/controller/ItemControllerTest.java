package com.example.todolist.controller;

import com.example.todolist.domain.Item;
import com.example.todolist.domain.ToDoList;
import com.example.todolist.service.ItemService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ItemControllerTest {

  @InjectMocks
  private ItemController itemController;

  @Mock
  private ItemService itemService;

  private Item item1;
  private Item item2;

  @BeforeEach
  void setUp() {
    ToDoList todoList = new ToDoList();
    todoList.setId(1L);

    item1 = new Item(1L, "Test item 1", "Description of the test item", todoList);
    item2 = new Item(2L, "Test item 2", "Description of the test item", todoList);
  }

  @Test
  public void getItemsByTodoListId() {
    List<Item> items = Arrays.asList(item1, item2);
    when(itemService.getItemsByTodoListId(1L)).thenReturn(items);

    List<Item> result = itemController.getItemsByTodoListId(1L);

    assertThat(result).isNotEmpty().hasSize(2);
    assertThat(result.get(0).getTitle()).isEqualTo("Test item 1");
    assertThat(result.get(1).getTitle()).isEqualTo("Test item 2");
  }

  @Test
  public void getItem() {
    when(itemService.getItemById(1L)).thenReturn(item1);

    Item item = itemController.getItem(1L);

    assertThat(item).isNotNull();
    assertThat(item.getTitle()).isEqualTo("Test item 1");
  }

  @Test
  public void createItem() {
    when(itemService.createItem(any(Item.class))).thenReturn(item1);

    Item item = itemController.createItem(item1);

    assertThat(item).isNotNull();
    assertThat(item.getTitle()).isEqualTo("Test item 1");
  }

  @Test
  public void updateItem() {
    when(itemService.updateItem(1L, item1)).thenReturn(item1);

    Item item = itemController.updateItem(1L, item1);

    assertThat(item).isNotNull();
    assertThat(item.getTitle()).isEqualTo("Test item 1");
  }

  @Test
  public void deleteItem() {
    itemController.deleteItem(1L);

    verify(itemService, times(1)).deleteItem(1L);
  }
}
