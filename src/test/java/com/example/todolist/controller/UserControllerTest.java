package com.example.todolist.controller;

import com.example.todolist.domain.User;
import com.example.todolist.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

  @InjectMocks
  private UserController userController;

  @Mock
  private UserService userService;

  private User user1;

  @BeforeEach
  void setUp() {
    user1 = new User();
    user1.setId(1L);
    user1.setUsername("testuser");
    user1.setEmail("test@example.com");
  }

  @Test
  public void getUserById() {
    when(userService.getUserById(1L)).thenReturn(user1);

    User user = userController.getUserById(1L);

    assertThat(user).isNotNull();
    assertThat(user.getId()).isEqualTo(1L);
    assertThat(user.getUsername()).isEqualTo("testuser");
    assertThat(user.getEmail()).isEqualTo("test@example.com");
  }

  @Test
  public void getUserByEmail() {
    when(userService.getUserByEmail("test@example.com")).thenReturn(user1);

    User user = userController.getUserByEmail("test@example.com");

    assertThat(user).isNotNull();
    assertThat(user.getId()).isEqualTo(1L);
    assertThat(user.getUsername()).isEqualTo("testuser");
    assertThat(user.getEmail()).isEqualTo("test@example.com");
  }

  @Test
  public void createUser() {
    when(userService.createUser(any(User.class))).thenReturn(user1);

    User user = userController.createUser(user1);

    assertThat(user).isNotNull();
    assertThat(user.getId()).isEqualTo(1L);
    assertThat(user.getUsername()).isEqualTo("testuser");
    assertThat(user.getEmail()).isEqualTo("test@example.com");
  }
}