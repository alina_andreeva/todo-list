package com.example.todolist.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.todolist.domain.ToDoList;
import com.example.todolist.domain.User;
import com.example.todolist.service.ToDoListService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ToDoListControllerTest {

  @InjectMocks
  private ToDoListController toDoListController;

  @Mock
  private ToDoListService toDoListService;

  private ToDoList toDoList1;
  private User user;

  @BeforeEach
  void setUp() {
    // Initialize User
    user = new User();
    user.setId(1L);
    user.setUsername("testuser");
    user.setEmail("test@example.com");

    // Initialize ToDoList
    toDoList1 = new ToDoList();
    toDoList1.setId(1L);
    toDoList1.setName("Test ToDoList");
    toDoList1.setUser(user);
    toDoList1.setItems(null);
  }

  @Test
  public void getToDoList() {
    when(toDoListService.getToDoListById(1L)).thenReturn(toDoList1);

    ToDoList toDoList = toDoListController.getToDoList(1L);

    assertThat(toDoList).isNotNull();
    assertThat(toDoList.getId()).isEqualTo(1L);
    assertThat(toDoList.getName()).isEqualTo("Test ToDoList");
  }

  @Test
  public void createToDoList() {
    when(toDoListService.createToDoList(any(ToDoList.class))).thenReturn(toDoList1);

    ToDoList toDoList = toDoListController.createToDoList(toDoList1);

    assertThat(toDoList).isNotNull();
    assertThat(toDoList.getId()).isEqualTo(1L);
    assertThat(toDoList.getName()).isEqualTo("Test ToDoList");
  }

  @Test
  public void deleteToDoList() {
    toDoListController.deleteToDoList(1L);

    verify(toDoListService, times(1)).deleteToDoList(1L);
  }
}